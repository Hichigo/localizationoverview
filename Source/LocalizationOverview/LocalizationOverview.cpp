// Copyright Epic Games, Inc. All Rights Reserved.

#include "LocalizationOverview.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, LocalizationOverview, "LocalizationOverview" );
