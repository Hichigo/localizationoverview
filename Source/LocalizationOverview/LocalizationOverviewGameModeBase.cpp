// Copyright Epic Games, Inc. All Rights Reserved.


#include "LocalizationOverviewGameModeBase.h"

#define LOCTEXT_NAMESPACE "MyNamespace"
// Test strings
const FText HelloWorld = NSLOCTEXT("MyOtherNamespace","HelloWorld","Hello World!");
const FText GoodbyeWorld = LOCTEXT("GoodbyeWorld","Goodbye World!");

#undef LOCTEXT_NAMESPACE