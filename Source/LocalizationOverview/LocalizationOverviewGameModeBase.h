// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LocalizationOverviewGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LOCALIZATIONOVERVIEW_API ALocalizationOverviewGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
};
